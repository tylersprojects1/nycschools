package com.example.nycschoolsexample.model

import retrofit2.Call
import retrofit2.http.GET

interface NYCApi {

    @GET("734v-jeq5.json")
    fun getSATData(): Call<List<CompleteSchool>>

    @GET("s3k6-pzi2.json")
    fun getAllSchoolsDetailed(): Call<List<CompleteSchool>>
}