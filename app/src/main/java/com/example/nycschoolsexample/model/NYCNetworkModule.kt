package com.example.nycschoolsexample.model

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NYCNetworkModule {
    var schoolSATData : MutableLiveData<List<CompleteSchool>> = MutableLiveData<List<CompleteSchool>>()
    var detailedSchoolData : MutableLiveData<List<CompleteSchool>> = MutableLiveData<List<CompleteSchool>>()
    var networkError: MutableLiveData<String> = MutableLiveData()

    private fun getRetrofit() : Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private val retrofitService: NYCApi by lazy {
        getRetrofit().create(NYCApi::class.java)
    }

    fun getSATData(){
        retrofitService.getSATData().enqueue(object : Callback<List<CompleteSchool>> {
            override fun onResponse(call: Call<List<CompleteSchool>>, response: Response<List<CompleteSchool>>) {
                schoolSATData.value = response.body()
            }

            override fun onFailure(call: Call<List<CompleteSchool>>, t: Throwable) {
                networkError.value = t.message
            }
        })
    }

    fun getAdditionalSchoolData(){
        retrofitService.getAllSchoolsDetailed().enqueue(object : Callback<List<CompleteSchool>>{
            override fun onFailure(call: Call<List<CompleteSchool>>, t: Throwable) {
                networkError.value = t.message
            }

            override fun onResponse(call: Call<List<CompleteSchool>>, response: Response<List<CompleteSchool>>) {
                detailedSchoolData.value = response.body()
            }
        })
    }
}