package com.example.nycschoolsexample.model

import com.google.gson.annotations.SerializedName

data class CompleteSchool (
    @SerializedName("school_email") val school_email : String,
    @SerializedName("website") val website : String,
    @SerializedName("phone_number") val phoneNumber : String,
    @SerializedName("total_students") val totalStudents : Int,
    @SerializedName("dbn") val dbn : String,
    @SerializedName("school_name") val schoolName : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("borough") val borough : String,
    @SerializedName("pct_stu_safe") val percentFeelSafe : Double,
    @SerializedName("num_of_sat_test_takers") val numSATTakers : String,
    @SerializedName("sat_critical_reading_avg_score") var avgReadingScore : String?,
    @SerializedName("sat_math_avg_score") var avgMathScore : String?,
    @SerializedName("sat_writing_avg_score") var avgWritingScore : String?,
    @SerializedName("overview_paragraph") var overview : String?
)