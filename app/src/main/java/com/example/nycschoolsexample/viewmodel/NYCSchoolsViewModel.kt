package com.example.nycschoolsexample.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nycschoolsexample.model.CompleteSchool
import com.example.nycschoolsexample.model.NYCNetworkModule

class NYCSchoolsViewModel : ViewModel() {

    private val networkModule: NYCNetworkModule by lazy {
        NYCNetworkModule()
    }

    private var mutableSchoolData: MutableLiveData<List<CompleteSchool>> = networkModule.schoolSATData
    var schoolSATData: LiveData<List<CompleteSchool>> = mutableSchoolData

    private var mutableDetailedSchoolData: MutableLiveData<List<CompleteSchool>> = networkModule.detailedSchoolData
    var detailedSchoolData: LiveData<List<CompleteSchool>> = mutableDetailedSchoolData

    private var mutableCompleteSchoolList = MutableLiveData<List<CompleteSchool>>()
    var completeSchoolList : LiveData<List<CompleteSchool>> = mutableCompleteSchoolList

    var mutableDisplayList = MutableLiveData<List<CompleteSchool>>()
    var schoolsToDisplay : LiveData<List<CompleteSchool>> = mutableDisplayList

    private var mediatorLiveData : MediatorLiveData<List<CompleteSchool>> = MediatorLiveData<List<CompleteSchool>>()
    val publicMediator = mediatorLiveData

    val error : LiveData<String> = networkModule.networkError

    init {
        mediatorLiveData.addSource(completeSchoolList){
            mutableDisplayList.value = it
        }

        mediatorLiveData.addSource(schoolSATData){
            // The data is only combined when neither are null,
            // aka, both network calls have returned. Could possibly be improved with coroutines?
            if (schoolSATData.value != null && detailedSchoolData.value != null){
                combineData()
            }
        }

        mediatorLiveData.addSource(detailedSchoolData){
            if (schoolSATData.value != null && detailedSchoolData.value != null){
                combineData()
            }
        }
    }

    fun combineData() {
        val satScores = mutableSchoolData.value
        val detailedSchools = mutableDetailedSchoolData.value

        val satAndDbnMap = satScores?.associateBy { it.dbn }
        if (detailedSchools != null) {
            for (school in detailedSchools){
                val satSchool = satAndDbnMap?.get(school.dbn)
                if (satSchool?.dbn == school.dbn) {
                    school.avgMathScore = satSchool.avgMathScore
                    school.avgReadingScore = satSchool.avgReadingScore
                    school.avgWritingScore = satSchool.avgWritingScore
                }
            }
        }
        mutableCompleteSchoolList.value = detailedSchools
    }

    fun getSchoolData(){
        networkModule.getSATData()
        networkModule.getAdditionalSchoolData()
    }

    fun formatEnglishScore(score: String?): String {
        val fixedScore = score?.toIntOrNull()
        return if (fixedScore != null){
            "SAT English: $score"
        }else{
            "SAT English: " + "n/a"
        }
    }

    fun formatMathScore(score: String?): String {
        val fixedScore = score?.toIntOrNull()
        return if (fixedScore != null){
            "SAT Math: $score"
        }else{
            "SAT Math: " + "n/a"
        }
    }

    fun formatWritingScore(score: String?): String {
        val fixedScore = score?.toIntOrNull()
        return if (fixedScore != null){
            "SAT Writing: $score"
        }else{
            "SAT Writing: " + "n/a"
        }
    }

    private fun getAggregateSATScore(school : CompleteSchool): Int?{
        val math = school.avgMathScore?.toIntOrNull()
        val reading = school.avgReadingScore?.toIntOrNull()
        val writing = school.avgWritingScore?.toIntOrNull()

        return if (math != null && reading != null && writing != null){
            math + reading + writing
        }else{
            null
        }
    }

    fun getAndformatAggregateSATScore(school : CompleteSchool): String {
        val aggregate = getAggregateSATScore(school)
        return if(aggregate != null){
            "Average SAT Score: $aggregate"
        }else{
            "Average SAT Score: n/a"
        }
    }

    fun formatStudentPopulation(totalStudents : Int) : String{
        return "Pop. $totalStudents"
    }

    fun formatSchoolSafety(safe: Double): String{
        val reducedDecimals = safe * 100
        val safetyFormatted = String.format("%.0f", reducedDecimals)
        return "$safetyFormatted% feel safe"
    }

    fun sortBySafety(schools: List<CompleteSchool>?): List<CompleteSchool>? {
        return schools?.sortedWith(compareByDescending { it.percentFeelSafe })
    }

    fun sortSchoolsBySAT(schools: List<CompleteSchool>?): List<CompleteSchool>?{
        var sorted = schools?.sortedWith(compareByDescending { (it.avgMathScore + it.avgReadingScore + it.avgWritingScore) })
        sorted = sorted?.filter { it.avgMathScore != null && it.avgMathScore != "s" }
        sorted = sorted?.filter { it.avgReadingScore != null && it.avgReadingScore != "s"}
        sorted = sorted?.filter { it.avgWritingScore != null && it.avgWritingScore != "s"}
        return sorted
    }

    fun sortByNeighborhood(schools: List<CompleteSchool>?): List<CompleteSchool>? {
        return schools?.sortedWith(compareBy { it.borough })
    }

    fun displaySortedBySAT(schools: List<CompleteSchool>?){
        mutableDisplayList.value = sortSchoolsBySAT(schools)
    }

    fun displaySortedByNeighborhood(schools: List<CompleteSchool>?){
        mutableDisplayList.value = sortByNeighborhood(schools)
    }

    fun displaySortedBySafety(schools: List<CompleteSchool>?){
        mutableDisplayList.value = sortBySafety(schools)
    }

    fun applyFilters(requiredSAT : Int, requiredSafety : Int, desiredBoroughs : List<String>){
        var originalList: List<CompleteSchool>? = completeSchoolList.value ?: return

        originalList = filterBySAT(requiredSAT, originalList!!)
        originalList = filterBySafety(requiredSafety, originalList)
        originalList = filterByBorough(desiredBoroughs, originalList)
        mutableDisplayList.value = originalList
    }

    fun filterBySAT(requiredSAT: Int, list : List<CompleteSchool>): List<CompleteSchool> {
        val cleanedList = list.filter { getAggregateSATScore(it) != null }
        return cleanedList.filter { getAggregateSATScore(it)!! >= requiredSAT }
    }

    fun filterBySafety(requiredSafety: Int, list: List<CompleteSchool>): List<CompleteSchool> {
        return list.filter { it.percentFeelSafe * 100 >= requiredSafety }
    }

    fun filterByBorough(desiredBoroughs: List<String>, list: List<CompleteSchool>): List<CompleteSchool> {
        var cleanedList: List<CompleteSchool> = list

        cleanedList = cleanedList.filter { it.borough != null }

        if (!desiredBoroughs.contains("Man")){
            cleanedList = list.filterNot { it.borough.contains("MANHATTAN", true)}
        }
        if (!desiredBoroughs.contains("Brk")){
            cleanedList = cleanedList.filterNot { it.borough.contains("BROOKLYN", true)}
        }
        if (!desiredBoroughs.contains("Brx")){
            cleanedList = cleanedList.filterNot { it.borough.contains("BRONX", true)}
        }
        if (!desiredBoroughs.contains("Sta")){
            cleanedList = cleanedList.filterNot { it.borough.contains("STATEN IS", true)}
        }
        if (!desiredBoroughs.contains("Qnn")){
            cleanedList = cleanedList.filterNot { it.borough.contains("QUEENS", true)}
        }
        return cleanedList
    }
}