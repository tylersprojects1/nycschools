package com.example.nycschoolsexample.recyclerview

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.school_summary.view.*

class SchoolViewHolder(itemView: View) : ViewHolder(itemView) {
    val name : TextView = itemView.name_summary
    val SATTotal : TextView = itemView.SAT_total
    val borough : TextView = itemView.summary_borough
    val students : TextView = itemView.summary_population
    val safetyRating : TextView = itemView.safety_summary
    val summaryLayout : ConstraintLayout = itemView.summary_layout
}