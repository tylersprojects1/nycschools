package com.example.nycschoolsexample.recyclerview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nycschoolsexample.R
import com.example.nycschoolsexample.view.SchoolDetailFragment
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel
import kotlinx.android.synthetic.main.fragment_recycler_view.*

class RecyclerViewFragment: Fragment(),
    NycRecyclerViewAdapter.OnSchoolSelectedListener {

    lateinit var adapter : NycRecyclerViewAdapter

    private val viewModel : NYCSchoolsViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NYCSchoolsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter =
            NycRecyclerViewAdapter(
                viewModel,
                viewModel.completeSchoolList.value!!,
                this
            )
        setUpRecyclerView(adapter)
        setUpObservables()
    }

    private fun setUpRecyclerView(nycAdapter: NycRecyclerViewAdapter){
        NYCRecyclerView.apply {
            setHasFixedSize(true)
            adapter = nycAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onSchoolClicked(position: Int) {
        val fragment = SchoolDetailFragment()
        val args = Bundle()
        args.putInt("position", position)
        fragment.arguments = args
        requireActivity().supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(R.id.contentFrame, fragment)
            .commit()
    }

    private fun setUpObservables(){
        viewModel.schoolsToDisplay.observe(this, Observer {
            adapter.updateData(it)
            adapter.notifyDataSetChanged()
        })
    }
}