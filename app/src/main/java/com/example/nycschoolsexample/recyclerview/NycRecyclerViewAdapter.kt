package com.example.nycschoolsexample.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschoolsexample.R
import com.example.nycschoolsexample.model.CompleteSchool
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel

class NycRecyclerViewAdapter(private val viewModel : NYCSchoolsViewModel, var schools : List<CompleteSchool>, private val clickListener : OnSchoolSelectedListener) : RecyclerView.Adapter<SchoolViewHolder>() {
    interface OnSchoolSelectedListener {
        fun onSchoolClicked(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.school_summary, parent, false)
        return SchoolViewHolder(v)
    }

    fun updateData(data : List<CompleteSchool>){
        schools = data
    }

    override fun getItemCount(): Int {
        return schools.size
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        setHolderText(holder, position)
        holder.summaryLayout.setOnClickListener {
            clickListener.onSchoolClicked(holder.adapterPosition)
        }
    }

    fun setHolderText(holder: SchoolViewHolder, position: Int){
        holder.name.text = schools.get(position).schoolName
        holder.SATTotal.text = viewModel.getAndformatAggregateSATScore(schools.get(position))
        holder.students.text = viewModel.formatStudentPopulation(schools.get(position).totalStudents)
        holder.borough.text = schools.get(position).borough
        holder.safetyRating.text = viewModel.formatSchoolSafety(schools.get(position).percentFeelSafe)
    }
}