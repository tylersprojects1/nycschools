package com.example.nycschoolsexample.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.nycschoolsexample.R
import com.example.nycschoolsexample.model.CompleteSchool
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.data.geojson.GeoJsonLayer

class MapsFragment : Fragment() {

    private lateinit var gMap: GoogleMap
    private lateinit var schoolDistrictLayer : GeoJsonLayer

    private val viewModel : NYCSchoolsViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NYCSchoolsViewModel::class.java)
    }

    private val callback = OnMapReadyCallback { googleMap ->
        gMap = googleMap
        addSchoolIconsAndFocusCamera()
        addDistrictsToMap()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun addSchoolIconsAndFocusCamera(){
        val schools: List<CompleteSchool> = viewModel.schoolsToDisplay.value ?: return

        for (school in schools){
            val latLng = LatLng(school.latitude, school.longitude)
            gMap.addMarker(MarkerOptions().position(latLng).title(school.schoolName))
        }

        val latLng = LatLng(40.7485, -73.9466)
        gMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))
    }

    private fun addDistrictsToMap(){
        schoolDistrictLayer = GeoJsonLayer(gMap, R.raw.simplified_school_districts, requireContext())
        schoolDistrictLayer.addLayerToMap()
    }
}