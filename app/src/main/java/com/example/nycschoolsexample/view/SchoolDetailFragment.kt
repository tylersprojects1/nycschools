package com.example.nycschoolsexample.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.nycschoolsexample.R
import com.example.nycschoolsexample.model.CompleteSchool
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel
import kotlinx.android.synthetic.main.fragment_school_detail.*

private const val ARG_POSITION = "position"

class SchoolDetailFragment : Fragment() {

    private var position = -1
    private var school: CompleteSchool? = null
    private val viewModel : NYCSchoolsViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NYCSchoolsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            position = it.getInt(ARG_POSITION)
        }
        if (position != -1){
            school = viewModel.completeSchoolList.value?.get(position)
        }
    }

    private fun setUpUi(school: CompleteSchool?){
        if (school == null){
            return
        }

        phoneNumber.text = school.phoneNumber
        email.text = school.school_email
        schoolName.text = school.schoolName
        math.text = viewModel.formatMathScore(school.avgMathScore)
        writing.text = viewModel.formatWritingScore(school.avgWritingScore)
        reading.text = viewModel.formatEnglishScore(school.avgReadingScore)
        overview.text = school.overview
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_school_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUi(school)
    }
}