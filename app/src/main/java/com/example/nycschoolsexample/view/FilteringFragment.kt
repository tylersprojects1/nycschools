package com.example.nycschoolsexample.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.nycschoolsexample.R
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel
import kotlinx.android.synthetic.main.filter_form.*

class FilteringFragment : Fragment() {

    private val viewModel : NYCSchoolsViewModel by lazy {
        ViewModelProvider(requireActivity()).get(NYCSchoolsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.filter_form, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpListeners()
    }

    private fun setUpListeners(){
        SAT_seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                SAT_position.text = p1.toString()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })

        safety_seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                safety_position.text = p1.toString()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })

        manhattan_checkbox.isChecked = true
        brooklyn_checkbox.isChecked = true
        bronx_checkbox.isChecked = true
        staten_island_checkbox.isChecked = true
        queens_checkbox.isChecked = true

        setListener(manhattan_checkbox)
        setListener(staten_island_checkbox)
        setListener(queens_checkbox)
        setListener(bronx_checkbox)
        setListener(brooklyn_checkbox)

        filterButton.setOnClickListener {
            applyFilters()
        }
    }

    fun setListener(checkBox: CheckedTextView){
        checkBox.setOnClickListener {
            if (checkBox.isChecked){
                checkBox.setBackgroundColor(Color.WHITE)
                checkBox.setTextColor(Color.BLACK)
                checkBox.isChecked = false
            }else{
                checkBox.setBackgroundColor(Color.WHITE)
                checkBox.setTextColor(Color.BLACK)
                checkBox.isChecked = true
            }
        }
    }

    private fun applyFilters(){
        val requiredSATScore = SAT_seek.progress
        val requiredSafety = safety_seek.progress
        val desiredBoroughs = mutableListOf<String>()

        if (manhattan_checkbox.isChecked) {
            desiredBoroughs.add("Man")
        }
        if (bronx_checkbox.isChecked){
            desiredBoroughs.add("Brx")
        }
        if (brooklyn_checkbox.isChecked){
            desiredBoroughs.add("Brk")
        }
        if (queens_checkbox.isChecked){
            desiredBoroughs.add("Qnn")
        }
        if (staten_island_checkbox.isChecked){
            desiredBoroughs.add("Sta")
        }

        viewModel.applyFilters(requiredSATScore, requiredSafety, desiredBoroughs)
        requireActivity().onBackPressed()
    }
}