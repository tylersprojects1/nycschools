package com.example.nycschoolsexample.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.nycschoolsexample.R
import com.example.nycschoolsexample.recyclerview.RecyclerViewFragment
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel

class MainActivity : AppCompatActivity(){

    private val viewModel : NYCSchoolsViewModel by lazy {
        ViewModelProvider(this).get(NYCSchoolsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setContentView(R.layout.activity_main)
        setUpObservables()
        viewModel.getSchoolData()
    }

    private fun setUpObservables(){
        viewModel.completeSchoolList.observe(this, Observer {
            val fragment =
                RecyclerViewFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.contentFrame, fragment)
                .commit()
        })

        //Room for future use? Current purpose: MediatorLiveData must be observed to be active
        viewModel.publicMediator.observe(this, Observer {})

        viewModel.error.observe(this, Observer {
            Toast.makeText(this, "Sorry, we're having trouble retrieving data", Toast.LENGTH_SHORT).show()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.sorting_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun viewSchoolsBySafety(m: MenuItem?) {
        viewModel.displaySortedBySafety(viewModel.schoolsToDisplay.value)
    }

    fun viewSchoolsByNeighborhood(m: MenuItem?){
        viewModel.displaySortedByNeighborhood(viewModel.schoolsToDisplay.value)
    }

    fun viewSchoolsBySATScore(m: MenuItem?){
        viewModel.displaySortedBySAT(viewModel.schoolsToDisplay.value)
    }

    fun showOnMap(m: MenuItem?){
        val fragment = MapsFragment()
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(R.id.contentFrame, fragment)
            .commit()
    }

    fun openFilterMenu(m: MenuItem?){
        val fragment = FilteringFragment()
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(R.id.contentFrame, fragment)
            .commit()
    }
}