package com.example.nycschoolsexample

import com.example.nycschoolsexample.model.CompleteSchool
import com.example.nycschoolsexample.viewmodel.NYCSchoolsViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NYCSchoolsViewModelTests {

    private lateinit var viewModel : NYCSchoolsViewModel
    private lateinit var schools : List<CompleteSchool>

    @Before
    fun setup(){
        viewModel = NYCSchoolsViewModel()

        val school1 = CompleteSchool(
            school_email = "test@test.com",
            website = "www.test.com",
            phoneNumber = "939-343-3421",
            totalStudents = 10000,
            dbn = "39432d",
            schoolName = "Manhattan Institute of Fakery",
            latitude = 343.43,
            longitude = 343.51,
            borough = "Manhattan",
            percentFeelSafe = .23,
            numSATTakers = "900",
            avgMathScore = "360",
            avgReadingScore = "370",
            avgWritingScore = "310",
            overview = "This is a fine place!")

        val school2 = CompleteSchool(
            school_email = "test2@test.com",
            website = "www.test2.com",
            phoneNumber = "943-343-3421",
            totalStudents = 1500,
            dbn = "24332d",
            schoolName = "Queens Institute of Fakery",
            latitude = 253.43,
            longitude = 42.51,
            borough = "Queens",
            percentFeelSafe = .73,
            numSATTakers = "500",
            avgMathScore = "660",
            avgReadingScore = "560",
            avgWritingScore = "230",
            overview = "This is an ok place!")

        val school3 = CompleteSchool(
            school_email = "test3@test.com",
            website = "www.test3.com",
            phoneNumber = "022-343-3421",
            totalStudents = 10000,
            dbn = "023212d",
            schoolName = "Brooklyn Institute of Fakery",
            latitude = 13.43,
            longitude = 142.51,
            borough = "Brooklyn",
            percentFeelSafe = .93,
            numSATTakers = "5000",
            avgMathScore = "760",
            avgReadingScore = "760",
            avgWritingScore = "630",
            overview = "This is a great place!")
        schools = listOf(school1, school2, school3)
    }

    @Test
    fun testSATSorting(){
        val sorted = viewModel.sortSchoolsBySAT(schools)

        val aggregate1 = sorted?.get(0)?.avgMathScore + sorted?.get(0)?.avgWritingScore + sorted?.get(0)?.avgReadingScore
        val aggregate2 = sorted?.get(1)?.avgMathScore + sorted?.get(1)?.avgWritingScore + sorted?.get(1)?.avgReadingScore
        val aggregate3 = sorted?.get(2)?.avgMathScore + sorted?.get(2)?.avgWritingScore + sorted?.get(2)?.avgReadingScore

        assert(aggregate1 >= aggregate2)
        assert(aggregate2 >= aggregate3)
        assert(aggregate1 != null)
    }

    @Test
    fun testSortBySafety(){
        val sorted = viewModel.sortBySafety(schools)
        assert(sorted?.get(0)?.percentFeelSafe != null)
        assert(sorted?.get(1)?.percentFeelSafe != null)
        assert(sorted?.get(1)?.percentFeelSafe != null)
        assert(sorted?.get(0)?.percentFeelSafe!! >= sorted.get(1).percentFeelSafe)
        assert(sorted.get(1).percentFeelSafe >= sorted.get(2).percentFeelSafe)
    }

    @Test
    fun testSortByNeighborhood(){
        val sorted = viewModel.sortByNeighborhood(schools)
        assert(sorted?.get(0)?.borough.equals("Brooklyn"))
        assert(sorted?.get(1)?.borough.equals("Manhattan"))
        assert(sorted?.get(2)?.borough.equals("Queens"))
    }

    @Test
    fun testSATFilter(){
        assert(viewModel.filterBySAT(1000, schools).containsAll(schools))
        assert(viewModel.filterBySAT(1200, schools).size == 2)
        assert(viewModel.filterBySAT(1900, schools).size == 1)
        assert(viewModel.filterBySAT(2400, schools).isEmpty())
    }

    @Test
    fun testSafetyFilter(){
        assert(viewModel.filterBySafety(0, schools).containsAll(schools))
        assert(viewModel.filterBySafety(50, schools).size == 2)
        assert(viewModel.filterBySafety(80, schools).size == 1)
        assert(viewModel.filterBySafety(100, schools).isEmpty())
    }

    @Test
    fun testBoroughFilter(){
        val list1 = listOf<String>("Man", "Brx")
        val list2 = listOf<String>("Qnn", "Sta")
        val list3 = listOf<String>("Brk", "Man")
        val list4 = listOf<String>()

        assert(!viewModel.filterByBorough(list1, schools).contains(schools.get(2)))
        assert(viewModel.filterByBorough(list1, schools).contains(schools.get(0)))

        assert(!viewModel.filterByBorough(list2, schools).contains(schools.get(0)))
        assert(!viewModel.filterByBorough(list2, schools).contains(schools.get(2)))
        assert(viewModel.filterByBorough(list2, schools).contains(schools.get(1)))

        assert(viewModel.filterByBorough(list3, schools).contains(schools.get(0)))
        assert(viewModel.filterByBorough(list3, schools).contains(schools.get(2)))
        assert(!viewModel.filterByBorough(list3, schools).contains(schools.get(1)))

        assert(!viewModel.filterByBorough(list4, schools).contains(schools.get(0)))
        assert(!viewModel.filterByBorough(list4, schools).contains(schools.get(1)))
        assert(!viewModel.filterByBorough(list4, schools).contains(schools.get(2)))
    }


    @Test
    fun testPopulationFormatting(){
        val formattedPopulation = viewModel.formatStudentPopulation(500)
        assert(formattedPopulation == "Pop. 500")
    }

    @Test
    fun testFormatEnglishScore(){
        assert(viewModel.formatEnglishScore("450") == "SAT English: 450")
        assert(viewModel.formatEnglishScore(null) ==  "SAT English: n/a")
    }

    @Test
    fun testFormatMathScore(){
        assert(viewModel.formatMathScore("500") == "SAT Math: 500")
        assert(viewModel.formatMathScore(null) == "SAT Math: n/a")
    }

    @Test
    fun testFormatWritingScore(){
        assert(viewModel.formatWritingScore("600") == "SAT Writing: 600")
        assert(viewModel.formatWritingScore(null) == "SAT Writing: n/a")
    }

    @Test
    fun testAggregateSATFormatting(){
        assert(viewModel.getAndformatAggregateSATScore(schools.get(0)) == "Average SAT Score: 1040")
        assert(viewModel.getAndformatAggregateSATScore(schools.get(1)) == "Average SAT Score: 1450")
        assert(viewModel.getAndformatAggregateSATScore(schools.get(2)) == "Average SAT Score: 2150")
    }

    @Test
    fun testSafetyFormatting(){
        assert(viewModel.formatSchoolSafety(.23) == "23% feel safe")
    }
}
